# -*- coding: utf-8 -*-
"""
:summary:

:author: Francis.horsman@gmail.com
"""

import unittest

from grouper import grouper, MalformedInputData
from grouper.core import _result


class Test(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_all(self):
        A = [[5, 4, 4],
             [4, 3, 4],
             [3, 2, 4],
             [2, 2, 2],
             [3, 3, 4],
             [1, 4, 4],
             [4, 1, 1]]

        result = grouper(data=A)
        assert result.ready is False
        result.validate
        result()
        assert result.ready is True

        rr = [i for i in result]
        self._check_result(rr)

        assert result.unique == 11
        assert result.min == 6
        assert result.max == 2

        # lazy initialization using the supplied test vector:
        result1 = grouper(parse=True, validate=True)
        assert result1.ready is False
        result1(data=A)
        rr = [i for i in result1]
        self._check_result(rr)

        assert len(result1) == 11
        assert result.unique == 11
        assert result.min == 6
        assert result.max == 2

        # check equality:
        assert result == result1

        # check non-equality:
        g = grouper()
        assert not g == object()
        assert g != result1

        syn = grouper.synopsis(A)
        assert isinstance(syn, _result)
        assert len(syn.unique_items) == 11
        assert len(syn.min_items) == 6
        assert len(syn.max_items) == 2

        r = grouper(data=A, parse=True)

        assert r.ready is True

        rr = [i for i in r]
        self._check_result(rr)

        assert r.unique == 11
        assert r.min == 6
        assert r.max == 2

    @staticmethod
    def _check_result(result):
        assert len(result) == 11
        assert result[0] == [(0, 0)]
        assert result[1] == [(0, 1), (0, 2), (1, 2), (2, 2)]
        assert result[2] == [(1, 0)]
        assert result[3] == [(1, 1)]
        assert result[4] == [(2, 0)]
        assert result[5] == [(2, 1), (3, 1), (3, 0), (3, 2)]
        assert result[6] == [(4, 0), (4, 1)]
        assert result[7] == [(4, 2), (5, 2), (5, 1)]
        assert result[8] == [(5, 0)]
        assert result[9] == [(6, 0)]
        assert result[10] == [(6, 1), (6, 2)]

    def test_malformed(self):
        a = [[1, 2], [3, 4], [5, 6, 7]]
        self.assertRaises(MalformedInputData,
                          lambda: grouper(data=a, validate=True))

        g = grouper(data=a, validate=False)
        self.assertRaises(MalformedInputData,
                          lambda: g.validate)

        g = grouper(data=a, validate=False)
        self.assertRaises(MalformedInputData,
                          lambda: g())

        try:
            g()
        except MalformedInputData as err:
            str(err)
        else:
            assert False

    def test_values_when_not_ready(self):
        g = grouper()
        assert g.min == 0
        assert g.max == 0
        assert g.unique == 0

    def test_str(self):
        A = [[5, 4, 4],
             [4, 3, 4],
             [3, 2, 4],
             [2, 2, 2],
             [3, 3, 4],
             [1, 4, 4],
             [4, 1, 1]]
        g = grouper(A)
        assert str(g) == 'grouper(7, 3)'

        g()
        assert str(g) == 'grouper(7, 3, unique=11, max=2, min=6)'

if __name__ == '__main__':  # pragma no cover
    unittest.main()
