.. :changelog:

Changelog
=========

0.0.7 2015-01-09
----------------
* Added manage, bumped to v0.0.7

0.0.6 2015-01-09
----------------
* Tagger working.

0.0.5 2015-01-07
----------------
* Initial release.
