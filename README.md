# README #

Simple adjacent cell grouping algorithm in 2d array.

Take 2 dimensional array, where each element contains a (not necessarily unique) value (value is any object).
Now group the elements in the array if the element is the same as an adjacent element (vertically or horizontally), equivalence is achieved by equating the elements.

The array can be validated on demand or when parsing.


[ ![Codeship Status for sys-git/grouper](https://codeship.com/projects/056d6120-78b3-0132-5c9b-6e4e99cdd669/status?branch=master)](https://codeship.com/projects/55740)
[![Build Status](https://api.shippable.com/projects/54afbe1bd46935d5fbc1e90a/badge?branchName=master)](https://app.shippable.com/projects/54afbe1bd46935d5fbc1e90a/builds/latest)

[![Downloads](https://pypip.in/download/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Latest Version](https://pypip.in/version/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Supported Python versions](https://pypip.in/py_versions/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Supported Python implementations](https://pypip.in/implementation/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Development Status](https://pypip.in/status/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Wheel Status](https://pypip.in/wheel/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Egg Status](https://pypip.in/egg/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![Download format](https://pypip.in/format/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)
[![License](https://pypip.in/license/grouper/badge.svg)](https://pypi.python.org/pypi/grouper/)

### How do I get set up? ###

* **python setup.py install**
* Run the tests from source **python setup.py nosetests**
* Dependencies:  None
* How to run tests with all nosetests options set:  **./runtests.sh**
* Deployment instructions (coming soon):  **pip install grouper**



### Requirements ###

At least Python 2.6
Tested on Python 3.4.0

### Contribution guidelines ###

I accept pull requests.

### Who do I talk to? ###

* Francis Horsman:  **francis.horsman@gmail.com**

### Example ###

```
>>> from grouper import grouper

>>> a = grouper([['a','b','c'], ['c','d','c'], ['e','c','c']])

>>> a
grouper(3, 3)

>>> [i for i in a.values]
[[(0, 0]), [(0, 1)], [(0, 2), (1, 2), (2, 2), (2, 1)], [(1, 0)], [(1, 1)], [(2, 0)]]

>>> a.unique_items
[[(0, 0)], [(0, 1)], [(0, 2), (1, 2), (2, 2), (2, 1)], [(1, 0)], [(1, 1)], [(2, 0)]]
# These tuples represent the array indexes, ie: a[0][2] == 'c'

>>> print(a)
grouper(3, 3, unique=6, max=4, min=1)

>>> len(a)
6

>>> a.unique
6

>>> a.max
4

>>> a.min
1

>>> a.max_items
[ [(0, 2), (1, 2), (2, 2), (2, 1)] ]

>>> a.min_items
[ [(0, 0)], [(0, 1)], [(1, 0)], [(1, 1)], [(2, 0)] ]

>>> b = grouper([['a','b','c'], ['c','d','c'], ['e','c','c']])

>>> b
grouper(3, 3)

>>> a == b
True

```