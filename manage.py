# -*- coding: utf-8 -*-
"""
:summary: tag and commit a release.

:author: Francis.horsman@gmail.com
"""

from __future__ import print_function
import sys
from datetime import datetime as dt

from sh import python, git
import click

DEFAULT_TAG = False
DEFAULT_PUBLISH_PYPI = False
DEFAULT_PUSH = True
DEFAULT_HISTORY_FILE = 'meta/HISTORY.rst'
DEFAULT_DELIMITER = '========='
DEFAULT_META = """.. :changelog:

Changelog
%s

""" % DEFAULT_DELIMITER

HISTORY_COMMENT = """%s %s
----------------
* %s
"""


def get_current_version_from_vcs():
    current_version = python('setup.py', '--version').split('-')[0] or '0.0.1'
    print('Current tagged version: %s' % current_version)
    return current_version


def get_current_version_from_args(args):
    return python('setup.py', '--version').split('-')[0] or '0.0.1'


def commit_history(history_file):
    print('Adding history file: %s to git' % history_file)
    git('add', history_file)
    git('commit', '-am', '"Updating tag and history"')


def read_history(history_file):
    print('Reading history file: %s' % history_file)
    return open(history_file).readlines()


def set_current_version(new_version, msg='updating changelog'):
    print('Tagging with new version: %s' % new_version)
    return git('tag', new_version, '-m', msg)


def push_to_git(push_all=True, push_tags=False):
    if push_all:
        print('Pushing ALL to git')
        git('push', '--all')
    if push_tags:
        print('Pushing TAGS to git')
        git('push', '--tags')
    if not push_all and not push_tags:
        print('Pushing to git')
        git('push')


def get_new_version(args, current_version):
    if not current_version:
        if not args:
            sys.exit('Please specify a version')
        else:
            current_version = get_current_version_from_args(args)
    elif args:
        current_version = args[0]
        if len(args[0].split('.')) != 3:
            sys.exit('Please specify a version as arg[0]')
    tokens = current_version.split('.')
    tokens[2] = str(int(tokens[2]) + 1)
    return '.'.join(tokens)


class History(object):
    def __init__(self, lines, meta, delimiter):
        self.comments = []
        self.lines = self._strip_meta(lines, delimiter)
        self.meta = meta

    def add(self, new_version, comment, date=None):
        if date is None:
            date = dt.utcnow().strftime('%Y-%m-%d')
        self.comments.insert(0, self._new_comment(comment or '', date,
                                                  new_version))

    def _new_comment(self, comment, date, new_version):
        return HISTORY_COMMENT % (new_version, date, comment or '')

    def _strip_meta(self, lines, delimiter):
        for index, line in enumerate(lines):
            if delimiter in line:
                return lines[index + 1:]
        return lines

    def write(self, filename):
        lines = [self.meta]
        for i in self.comments:
            lines.append(i)
        lines.extend(self.lines)
        history = ''.join(lines)
        open(filename, 'w').write(history)


def rewrite_history(history_file, new_version, comment, date, meta, delimiter):
    print('Rewriting history: %s' % new_version)
    history = History(read_history(history_file), meta, delimiter)
    history.add(new_version, comment, date=date)
    history.write(history_file)
    commit_history(history_file)


def publish_to_pypi(new_version):
    print('Publishing %s to pypi' % new_version)
    # python('setup.py', 'build', 'sdist', 'bdist_egg', 'upload')


def build(new_version):
    print('Building %s' % new_version)
    python('setup.py', 'build', 'sdist', 'bdist_egg')


def manage(version=None, comment=None, date=None, file=None, meta=None,
           delimiter=None, push=None, pypi=None, tag=None):
    if tag:
        current_version = get_current_version_from_vcs()
        new_version = get_new_version(version, current_version)
        rewrite_history(file, new_version, comment, date, meta, delimiter)
        set_current_version(new_version)
    if push:
        push_to_git(push_all=True, push_tags=True)
    if pypi:
        if tag:
            # Only push tagged builds to pypi:
            publish_to_pypi(new_version)
    else:
        build(new_version)


@click.command()
@click.option('--version', default='',
              help='New version (optional - previous version will be incremented).')
@click.option('--comment', help='The changelog comment to add.')
@click.option('--date', default=dt.utcnow().strftime('%Y-%m-%d'),
              help='The changelog date to add.')
@click.option('--file', default=DEFAULT_HISTORY_FILE,
              help='The history text file to use.')
@click.option('--delimiter', default=DEFAULT_DELIMITER,
              help='The history file synopsis delimiter (prior to history entries).')
@click.option('--meta', default=DEFAULT_META,
              help='The history file synopsis (prior to the delimiter).')
@click.option('--push', default=DEFAULT_PUSH,
              help='Push the tag to git automatically.')
@click.option('--pypi', default=DEFAULT_PUBLISH_PYPI,
              help='Publish to pypi.')
@click.option('--tag', default=DEFAULT_TAG,
              help='Tag release.')
def main(version, comment, date, file, meta, delimiter, push, pypi, tag):
    return manage(version, comment, date, file, meta, delimiter, push, pypi,
                  tag)


if __name__ == '__main__':  # pragma no cover
    main()
