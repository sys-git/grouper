# -*- coding: utf-8 -*-

# copyright 2014, Francis Horsman, francis.horsman@gmail.com.

import collections

try:
    # xrange not defined in python 3.
    xrange
except NameError:
    xrange = range


class MalformedInputData(Exception):
    """
    The input two dimensional array is malformed somehow.
    """

    def __init__(self, a, row=None, col=None):
        """
        class instantiation

        :param a: The array that is malformed.
        :param row: Optional, the row where the malformation was discovered if
            available.
        :param col: Optional, the col where the malformation was discovered if
            available.
        """
        Exception.__init__(self, a, row, col)
        self.array = a
        self.row = row
        self.col = col

    def __str__(self):
        s = ['MalformedInputData']
        if self.row is not None or self.col is not None:
            s.append('(%s, %s)' % (self.row, self.col))
        return ''.join(s)

# synopsis of solution:
_result = collections.namedtuple('result',
                                 ('max_items', 'min_items', 'unique_items'))


class _parser(object):
    """
    implementation to solve the two dimensional array problem.
    """

    @staticmethod
    def get_array_size(a):
        """
        determine the number of rows and number of columns in the input array.

        :param a: The array to parse.
        :raise: ValueError - No input array specified.
        :raise: MalformedInputData - Array of inconsistent size.
        """
        if a is None:  # pragma no cover
            raise ValueError('No input array specified')
        try:
            num_rows = len(a)
            try:
                num_cols = len(a[0])
            except:
                # may be an array with only one col:
                num_cols = 0
            return num_rows, num_cols
        except Exception as e:  # pragma no cover
            raise MalformedInputData(a)

    @staticmethod
    def get(a, row, col):
        """
        get a cell at row, col

        :param a: The array to parse
        :param row: Zero based index of row to parse.
        :param col: Zero based index of col to parse.
        :return: The contents of the specified cell.
        """
        # raises IndexError if item doesn't exist (ie: out-of-bounds).
        num_rows, num_cols = _parser.get_array_size(a)

        # check the row, col to see if they're out-of-bounds:
        if row < 0 or col < 0 or row >= num_rows or col >= num_cols:
            # Fail-fast:
            raise IndexError(
                'Array access out of bounds: [%s][%s] from array: [%s][%s]' % (
                    row, col, num_rows, num_cols))
        return a[row][col]

    @staticmethod
    def get_adjacent(taken, a, row, col, e_value):
        """
        get the adjacent cells to the given cell with the same value

        :param taken: List of previously taken cells.
        :param a: The array to parse
        :param row: Zero based index of row to parse.
        :param col: Zero based index of col to parse.
        :param e_value: Adjacent cells must contain this value. Note: value can
        be any object that supports the __eq__ operator.
        :return: A list of cell tuples (row, col).
        """
        # gets all adjacent cells to r[row][col] that are of the same value
        cells = []
        # get adjacent cells from row, col from array: A not already in taken:
        for offsets in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            r = row + offsets[0]
            c = col + offsets[1]
            if (r, c) in taken:
                # cell already taken so we ignore it:
                continue
            try:
                value = _parser.get(a, r, c)
            except IndexError:
                # indicees are out-of-bounds, must be invalid so we don't care!
                continue
            if value == e_value:
                # got unused cell value to recursively check:
                cells.append((r, c))
        return cells

    @staticmethod
    def parse_cell(a, row, col, taken):
        """
        This parses a cell and gets all adjacent cells with the same value
        (recursively), not ideal because we could hit the interpreters recursion
        limit.

        :param a: The array to parse
        :param row: Zero based index of row to parse.
        :param col: Zero based index of col to parse.
        :param taken: List of previously taken cells.
        """
        stack = []

        # always take this cell:
        taken.append((row, col))
        try:
            value = _parser.get(a, row, col)
        except IndexError:  # pragma no cover
            # cell should always be valid at this point!
            raise MalformedInputData(a, row, col)
        # now get adjacent (row, col) tuples:
        cells = _parser.get_adjacent(taken, a, row, col, value)
        stack.append((row, col))
        if len(cells) == 0:
            # no more cells in this country:
            return stack
        for r, c in cells:
            # now parse the adjacent cells to this cell:
            stack.extend(_parser.parse_cell(a, r, c, taken))
        return stack

    @staticmethod
    def solution(data):
        """
        solve the input 2 dimensional array

        :note: Assumes all input data is valid- may be check at runtime if
        there's time!
        :param data: The array to parse.
        """
        if data is None:  # pragma no cover
            raise ValueError('No input array specified!')

        taken = []
        uniques = []

        num_rows, num_cols = _parser.get_array_size(data)

        # this gives us O(N * M) complexity in time:
        for row in xrange(num_rows):
            for col in xrange(num_cols):
                if (row, col) in taken:
                    # skip over cells already taken:
                    continue
                uniques.append(_parser.parse_cell(data, row, col, taken))
        return uniques

    @staticmethod
    def synopsis(data):
        """
        get a synopsis of the data

        :param data: Data as returned by _parser.solution()
        :return: dict( min_items=list(lists), max_items=list(lists) )
        """
        r = {'min_items': [0, []], 'max_items': [0, []],
             'unique_items': data}

        for i in data:
            l = len(i)
            min_ = r['min_items'][0]
            max_ = r['max_items'][0]

            if min_:
                if l < min_:
                    r['min_items'][1] = [i]
                    r['min_items'][0] = l
                elif l == min_:
                    r['min_items'][1].append(i)
                    r['min_items'][0] = l
            else:
                r['min_items'][1] = [i]
                r['min_items'][0] = l
            if max_:
                if l > max_:
                    r['max_items'][1] = [i]
                    r['max_items'][0] = l
                elif l == max_:
                    r['max_items'][1].append(i)
                    r['max_items'][0] = l
            else:
                r['max_items'][1] = [i]
                r['max_items'][0] = l

        for what in ['min_items', 'max_items']:
            r[what] = r[what][1]
        return r


class grouper(object):
    """
    external interface available to solve the two dimensional array problem.
    """

    def __init__(self, data=None, parse=False, validate=False):
        """
        object initialization.
        Can be lazily instantiated if required.
        There are only three ways to compute the result:
            1. set parse=True: in this __init__ method,
            2. call this instances: __call__ method,
            3. call this class as an iterator: __iter__.

        :param data: The two dimensional array to parse
        :param parse: True - parse immediately, False - parse on demand.
        :param validate: True - validate input array immediately,
            False - validate on demand.
        :return:
        """
        self.num_rows = 0
        self.num_cols = 0

        self._result = None
        self._data = None

        self._compute_size(data)
        if validate:
            grouper._validate(self._data)
        if parse and data is not None:
            self()

    @property
    def validate(self):
        """
        validate the input array in both dimensions.
        """
        self._validate(self._data)

    @staticmethod
    def _validate(data):
        num_rows, num_cols = grouper.get_array_size(data)
        for index, row in enumerate(data):
            if len(row) != num_cols:
                raise MalformedInputData(data, row=index)

    @staticmethod
    def get_array_size(a):
        """
        determine the number of rows and number of columns in the input array.

        :param a: The array to parse.
        :raise: ValueError - No input array specified.
        :raise: MalformedInputData - Array of inconsistent size.
        """
        return _parser.get_array_size(a)

    @staticmethod
    def synopsis(data):
        """
        get the synopsis for this array data.

        factory method to get the synopsis without the caller having to create
        an intermediate object.

        :param data: The array to parse.
        :return: _result
        """
        return _result(**_parser.synopsis(_parser.solution(data)))

    @property
    def ready(self):
        return self._result is not None

    @property
    def min(self):
        if self.ready:
            return len(self._result.min_items)
        return 0

    @property
    def max(self):
        if self.ready:
            return len(self._result.max_items)
        return 0

    @property
    def unique(self):
        if self.ready:
            return len(self._result.unique_items)
        return 0

    @property
    def values(self):
        """
        obtain the list of unique_items

        :return: list of unique items.
        """
        return self().unique_items

    def _compute_size(self, data):
        self._data = data or []
        self._result = None
        self.num_rows, self.num_cols = self.get_array_size(self._data)

    def __call__(self, data=None, validate=True):
        """
        iterate over the array and extract the data.

        :param data: optional - new data to use (lazy instantiation).
        """
        if data is not None:
            self._compute_size(data or [])
        if validate is True:
            self.validate
        if self._result is None:
            self._result = grouper.synopsis(self._data)
        return self._result

    def __iter__(self):
        """
        iterate over the unique items.
        """
        return iter(self.values)

    def __len__(self):
        if self._result is not None:
            return len(self._result.unique_items)

    def __str__(self):
        s = list('grouper(')
        s.append('%s, ' % self.num_rows)
        s.append('%s' % self.num_cols)
        if self._result is not None:
            r = self._result
            s.append(', unique=%s, max=%s, min=%s' % (
                self.unique, self.max, self.min))
        s.append(')')
        return ''.join(s)

    def __eq__(self, other):
        """
        equality of unique_items

        Two groupers are the same, if and only if:
            1. A result is computed for each,
            2. The unique_items are the same (order irrelevant).
        :param other: The other grouper instance to compare with.
        :return: True - This and other instance are the same, False - otherwise.
        """
        if not isinstance(other, grouper) or not self.ready or not other.ready:
            return False
        return sorted(self.values) == sorted(other.values)


if __name__ == '__main__':  # pragma no cover
    pass
