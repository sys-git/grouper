# -*- coding: utf-8 -*-

__author__ = 'Francis Horsman'
__url__ = 'https://bitbucket.org/sys-git/grouper'
__email__ = 'francis.horsman@gmail.com'
__short_description__ = 'A pure-python adjacent cell amalgamator for 2D arrays'

from .core import MalformedInputData, grouper

from .version import get_versions

__version__ = get_versions()['version']
del get_versions

if __name__ == '__main__':  # pragma: no cover
    print(__short_description__)

